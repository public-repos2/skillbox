#/bin/bash

cat <<EOF>> 00-init.tf
provider "yandex" {
  token     = "$OAUTH_KEY"
  folder_id = "$FOLDER_ID"
  zone      = "ru-central1-a"
}

terraform {
  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "tos"
    region     = "us-east-1"
    key        = "tstate/skill.tfstate"
    access_key = "$ACCESS_KEY"
    secret_key = "$SECRET_KEY"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

terraform {
  required_providers {
    yandex = {
      source = "terraform-providers/yandex"
    }
  }
  required_version = ">= 0.13"
}


EOF