# Complex Infrastructure As Code Example

# Run you tetris game in kubernetes ! ))
 
# Workflow
 
- User commit changes to master
- Run auto build and push image to registry
- Auto make terraform plan
- Manual review plan and apply (only maintainer)
- Manual deploy to kubernetes (only maintainer)
- Manual destroy all infra (only maintainer)
 
# Using tools:
 
- Terraform, to create and deploy infrastructure
- Kubernetes as service (yandex provider), - place to deploy apps.
- Continuous Deploy and Delivery tool, - gitlab ci
 
 
# Disclaimer
 
This repo not for production, only POC. But several production ready patterns followed:
 
##  Security
 
- All sensitive variables placed to mark variables in gitlab
- All sensitive data  added .gitinore
